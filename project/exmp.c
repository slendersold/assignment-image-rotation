/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "exmp.h"
#include <inttypes.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

void pls(struct image img, struct image new_img,uint64_t w,uint64_t h)
{
    set_pixel(&new_img, h, w, get_pixel(&img, w, h));
}
void mns(struct image img, struct image new_img,uint64_t w,uint64_t h)
{
    set_pixel(&new_img, h, w, get_pixel(&img, w, (img.height - h)));
}
