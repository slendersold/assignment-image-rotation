//
// Created by slendersold on 22.01.2021.
//
#include "bmp_input.h"
#include "util.h"

#include <inttypes.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>

static enum read_status check_header(FILE* in, struct bmp_header* h){
    if(!fread( h, sizeof( struct bmp_header ), 1, in ) ||
       (h->bfType!=0x4d42 && h->bfType!=0x4349 && h->bfType!=0x5450))
    {
        return READ_INVALID_HEADER_TYPE;
    };

    if( h->bfReserved != 0    ||
        h->biPlanes   != 1    ||
        (h->biSize!=40 && h->biSize!=108 && h->biSize!=124)||
        h->biWidth <1 ||
        h->biHeight<1 ||
        h->biBitCount    != 24 ||
        h->biCompression !=  0
            )
    {
        return READ_INVALID_HEADER_SETTINGS;
    }
    else {
        return READ_OK;
    }
}

enum read_status from_bmp(FILE* in, struct image* img) {

    struct bmp_header h = {0};
    const enum read_status rs = check_header(in, &h);

    if(READ_OK != rs)
    {
        return rs;
    };

    const uint32_t hg = h.biHeight;
    const uint32_t w = h.biWidth;
    img->width = w;
    img->height = hg;
    const size_t pad = padding(w);

    struct pixel* p = malloc(sizeof (struct pixel)*hg*w);

    size_t cnt = 0;

    for (uint32_t i = 0; i < hg; i++)
    {
        cnt += fread(p + (w * i), sizeof(struct pixel), w, in);
        fseek(in, pad, SEEK_CUR);
    }

    if (cnt != w*hg){
        return READ_INVALID_BYTES;
    }

    img->data = p;
    return READ_OK;
}
