/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "rotate.h"
#include "util.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

struct image process (struct image img ,R* const action)
{
    struct image new_img = new_image_empty(img.height,img.width );

    for (uint32_t x = 0; x < img.width; ++x) {
        for (uint32_t y = 0; y < img.height; ++y) {
            action(img, new_img, x, y);
        }
    }
    return new_img;
}