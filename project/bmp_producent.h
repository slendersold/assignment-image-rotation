//
// Created by slendersold on 21.01.2021.
//

#ifndef PROJECT_BMP_PRODUCENT_H
#define PROJECT_BMP_PRODUCENT_H

#include "bmp_core.h"
#include "image.h"

enum write_status to_bmp( FILE* const out, struct image* const image);

#endif //PROJECT_BMP_PRODUCENT_H
