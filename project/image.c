/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "image.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

struct image new_image_empty(uint64_t width, uint64_t height) {
    return (struct image) {width, height, (struct pixel*) malloc(width*height* sizeof(struct pixel))};
}

void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel const p) {
    img->data[y*img->width + x] = p;
}

struct pixel get_pixel(struct image const* img, uint64_t x, uint64_t y) {
    return img->data[y*img->width + x];
}
