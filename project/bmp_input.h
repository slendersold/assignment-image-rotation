//
// Created by slendersold on 22.01.2021.
//

#ifndef PROJECT_BMP_INPUT_H
#define PROJECT_BMP_INPUT_H

#include "bmp_core.h"
#include "util.h"
enum read_status from_bmp(FILE* in, struct image* img);

#endif //PROJECT_BMP_INPUT_H
