#ifndef _UTIL_H_
#define _UTIL_H_

#include "image.h"

#include <stdint.h>

_Noreturn void err( const char* msg, ... );
void logg( const char* msg, ... );

enum rot_direction
{
    CLOCK,
    UNCLOCK
};

typedef void (R)(struct image img, struct image new_img,uint64_t i,uint64_t j);

enum read_status
{
    READ_OK = 0,
    READ_IN_PROGRESS,
    READ_INVALID_BYTES,
    READ_INVALID_HEADER_TYPE,
    READ_INVALID_HEADER_SETTINGS,
    READ_NOTHING,
    READ_INVALID_PATH
};

enum write_status
{
    WRITE_OK = 0,
    WRITE_TO_TRANSFORM,
    WRITE_IN_PROGRESS,
    WRITE_ERROR,
    WRITE_INVALID_PATH,
    WRITE_INVALID_BYTES
};

#endif
