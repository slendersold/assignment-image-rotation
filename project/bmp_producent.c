//
// Created by slendersold on 21.01.2021.

#include "bmp_producent.h"
#include <inttypes.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
static struct bmp_header make_header(struct image const *img){

    struct bmp_header bh={0};
    bh.bfType=0x4d42;
    bh.bfileSize=(img->width*img->height*sizeof(struct pixel))+padding((uint32_t)(img->width))+ sizeof(struct bmp_header);
    bh.bfReserved=0;
    bh.biSize=40;
    bh.bOffBits= sizeof(struct bmp_header);
    bh.biWidth=img->width;
    bh.biHeight=img->height;
    bh.biPlanes=1;
    bh.biBitCount=24;
    bh.biCompression=0;

    bh.bfileSize=bh.bfileSize-bh.bOffBits;
    return bh;

}

enum write_status to_bmp( FILE* const out, struct image* const image){


    const struct bmp_header h = make_header(image);

    const uint64_t hg = image->height;
    const uint64_t w = image->width;
    const uint32_t pad = padding(w);

    if(!fwrite(&h, sizeof(struct bmp_header), 1, out)) return WRITE_ERROR;
    size_t cnt = 0;

    const struct pixel* p =  image->data;

    const uint8_t fill[] = {0,0,0};

    for(uint32_t i = 0; i < hg; i++)
    {
        cnt += fwrite( p + (w * i), sizeof(struct pixel)*w, 1, out);
        fwrite(fill, 1, pad, out);
    }

    if(cnt < hg){
        return WRITE_INVALID_BYTES;
    }
    return WRITE_OK;
}

