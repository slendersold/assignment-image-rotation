#include <stdbool.h>
#include <stdio.h>

//#include "bmp.h"
//#include "util.h"
#include "bmp_input.h"
#include "bmp_producent.h"
#include "exmp.h"
#include "rotate.h"

R* const choice []= {
        pls,
        mns
};

static const char* const read_message[] = {
        [READ_OK] = "Ok,KoMpUtEr \n",
        [READ_IN_PROGRESS] = "reading is starting \n",
        [READ_INVALID_BYTES] = "some bytes has been stolen while transporting. Read-express is really apologies for it\n",
        [READ_INVALID_HEADER_TYPE] = "wrong data in header: type is not bmp \n",
        [READ_INVALID_HEADER_SETTINGS] = "wrong data in header: size or something like that is wrong \n",
        [READ_NOTHING] = "nothing is here \n",
        [READ_INVALID_PATH] = "wrong in path \n"
};
static const char* const write_message[] = {
        [WRITE_OK] = "Writing process has successfully finished \n",
        [WRITE_TO_TRANSFORM] = "file is ready, starting transformation\n",
        [WRITE_IN_PROGRESS] = "writing is starting \n",
        [WRITE_ERROR] = "your HEADer has been damaged, new file is empty now, pls restart program or check permissions\n",
        [WRITE_INVALID_PATH] = "we can't create or reach out_file, check your permissions \n",
        [WRITE_INVALID_BYTES] = "a bunch pixels lost after writing\n"
};

void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}

int main( int argc, char** argv )
{
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );

    if (!argv[1]) err(read_message[READ_INVALID_PATH] );
    FILE* f = fopen( argv[1], "rb" );
    if (!f) err(read_message[READ_NOTHING]);

    struct image img = new_image_empty(0, 0);

    logg(read_message[READ_IN_PROGRESS]);
    enum read_status fb = from_bmp(f,&img);
    if(fb != READ_OK)
    {
        fclose( f );
        err(read_message[fb]);
    }
    logg(read_message[fb]);

    FILE* const out = fopen( "rev.bmp", "wb" );
    logg(write_message[WRITE_TO_TRANSFORM]);
    struct image new_img = process (img ,choice[CLOCK]);

    logg(write_message[WRITE_IN_PROGRESS]);

    enum write_status tb = to_bmp( out, &new_img);
    if(tb != WRITE_OK)
    {
        fclose( f );
        fclose( out );
        err(write_message[tb]);
    }
    logg(write_message[tb]);

    fclose( f );
    fclose( out );

//    struct bmp_header h = { 0 };
//    if (read_header_from_file( argv[1], &h )) {
//        bmp_header_print( &h, stdout );
//    }
//    else {
//        err( "Failed to open BMP file or reading header.\n" );
//    }

    return 0;
}
