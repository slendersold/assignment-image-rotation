/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   image_sheet.h
 * Author: slendersold
 *
 * Created on 21 января 2021 г., 1:43
 */

#ifndef IMAGE_SHEET_H
#define IMAGE_SHEET_H

#include <stdint.h>

struct pixel { 
    uint8_t b; 
    uint8_t g; 
    uint8_t r; };

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

struct image new_image_empty(uint64_t width, uint64_t height);
void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel const p);
struct pixel get_pixel(struct image const* img, uint64_t x, uint64_t y);


#endif /* IMAGE_SHEET_H */

